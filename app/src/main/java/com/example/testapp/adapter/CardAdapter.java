package com.example.testapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.testapp.DetailActivity;
import com.example.testapp.MainActivity;
import com.example.testapp.R;
import com.example.testapp.model.Card;
import com.example.testapp.utils.TappedListener;

import java.util.ArrayList;

import static com.example.testapp.utils.ConstantManager.ID;

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.CardViewHolder> implements Filterable{

    private Context context;
    private ArrayList<Card> fullCardList, filteredCardList;
    private TappedListener listener;
    private ArrayList<String> tappedList;
    private CardViewHolder viewHolder;

    public CardAdapter(Context context, ArrayList<Card> cardList, ArrayList<String> tappedListString, TappedListener listener) {
        fullCardList = cardList;
        filteredCardList = cardList;
        tappedList = tappedListString;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_card_view, parent, false);
        viewHolder = new CardViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CardViewHolder holder, final int position) {
        final Card card = filteredCardList.get(position);
        int value = Integer.parseInt(card.getProgress());

        if (tappedList.contains(card.getUuid())) {
            holder.itemContainer.setBackground(new ColorDrawable(ContextCompat.getColor(context, R.color.primary_light)));
        } else {
            holder.itemContainer.setBackground(new ColorDrawable(ContextCompat.getColor(context, R.color.colorWhite)));
        }

        holder.itemTitle.setText(card.getTitle());
        holder.itemStatus.setText(card.getStatus());

        holder.itemDate.setText(card.getDate());
        holder.itemProgressTxv.setText(card.getProgress());
        holder.yellowProgress.setProgress(value);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!MainActivity.multiSelect){
                    Intent detailActivity = new Intent(context, DetailActivity.class);
                    detailActivity.putExtra(ID, card.getUuid());
                    v.getContext().startActivity(detailActivity);
                } else {
                    listener.onTapped(card.getUuid(), position);
                    if (tappedList.contains(card.getUuid())) {
                        holder.itemContainer.setBackground(new ColorDrawable(ContextCompat.getColor(context, R.color.primary_light)));
                    } else {
                        tappedList.remove(card.getUuid());
                        holder.itemContainer.setBackground(new ColorDrawable(ContextCompat.getColor(context, R.color.colorWhite)));
                    }
                }
            }
        });


        holder.itemTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onTapped(card.getUuid(), position);
                if (tappedList.contains(card.getUuid())) {
                    holder.itemContainer.setBackground(new ColorDrawable(ContextCompat.getColor(context, R.color.primary_light)));
                } else {
                    tappedList.remove(card.getUuid());
                    holder.itemContainer.setBackground(new ColorDrawable(ContextCompat.getColor(context, R.color.colorWhite)));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (filteredCardList != null) {
            return filteredCardList.size();
        } else {
            return 0;
        }
    }

    public void addCard(Card card){
        filteredCardList.add(card);
        fullCardList.add(card);
        notifyItemInserted(filteredCardList.size());
    }

    public void addCard(int position, Card card){
        filteredCardList.add(position, card);
        notifyItemInserted(position);
    }

    public void setCard(int position, Card card){
        filteredCardList.set(position, card);
        fullCardList.set(position, card);
        notifyItemChanged(position);
    }

    public void removeCard(int position){
        filteredCardList.remove(position);
        notifyItemRemoved(position);
    }



    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String query = constraint.toString();

                if (query.isEmpty()){
                    filteredCardList = fullCardList;
                } else {
                    ArrayList<Card> matches = new ArrayList<>();

                    for (Card card : filteredCardList){
                        if (card.getTitle().toLowerCase().contains(query)
                                || card.getProgress().toLowerCase().contains(query)){
                            matches.add(card);
                        }
                    }
                    filteredCardList = matches;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredCardList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredCardList = (ArrayList<Card>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public void setList(ArrayList<Card> list) {
        filteredCardList = list;
        notifyDataSetChanged();
    }

    public class CardViewHolder extends RecyclerView.ViewHolder {

        public TextView itemTitle, itemProgressTxv, itemStatus, itemDate;
        public ProgressBar yellowProgress;
        public RelativeLayout itemContainer;


        public CardViewHolder(View itemView) {
            super(itemView);
            itemContainer = itemView.findViewById(R.id.itemContainer);
            itemTitle = itemView.findViewById(R.id.itemNameTxv);
            itemStatus = itemView.findViewById(R.id.itemStatus);
            itemDate = itemView.findViewById(R.id.itemDate);
            itemProgressTxv = itemView.findViewById(R.id.itemProgressTxv);
            yellowProgress = itemView.findViewById(R.id.itemProgress);
        }
    }

//    public Card getItem(int position){
//        return filteredCardList.get(position);
//    }

//    public void removeCard(Card card){
//        filteredCardList.remove(card);
//    }

}

