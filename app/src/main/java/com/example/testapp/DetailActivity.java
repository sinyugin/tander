package com.example.testapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.testapp.db.DatabaseManager;
import com.example.testapp.fragment.DetailFragment;
import com.example.testapp.model.Card;
import com.example.testapp.utils.ConstantManager;

import java.util.ArrayList;

import static com.example.testapp.utils.ConstantManager.ID;

public class DetailActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private ArrayList<Card> cardList;
    private FragmentManager fragMan;
    private DatabaseManager cardLab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        String uuid = getIntent().getStringExtra(ID);
        cardLab = DatabaseManager.getInstance();
        cardList = cardLab.getCards();
        fragMan = getSupportFragmentManager();
        viewPager = findViewById(R.id.viewPager);

        viewPager.setAdapter(new FragmentStatePagerAdapter(fragMan) {
            @Override
            public Fragment getItem(int position) {
                Card card = cardList.get(position);
                return DetailFragment.newInstance(card.getUuid());
            }

            @Override
            public int getCount() {
                return cardList.size();
            }
        });


        for (int i = 0; i < cardList.size(); i++){
            if (cardList.get(i).getUuid().equals(uuid)){
                viewPager.setCurrentItem(i);
                break;
            }
        }
        Log.d(ConstantManager.TAG, "DetailActivity onCreate");
    }

    @Override
    protected void onStart() {
        Log.d(ConstantManager.TAG, "DetailActivity onStart");
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(ConstantManager.TAG, "DetailActivity onResume");
    }

    @Override
    protected void onPause() {
        Log.d(ConstantManager.TAG, "DetailActivity onPause");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.d(ConstantManager.TAG, "DetailActivity onStop");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d(ConstantManager.TAG, "DetailActivity onDestroy");
        super.onDestroy();
    }

    @Override
    protected void onRestart() {
        Log.d(ConstantManager.TAG, "DetailActivity onRestart");
        super.onRestart();
    }

}
