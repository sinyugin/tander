package com.example.testapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.testapp.model.Card;
import com.example.testapp.model.DummyData;
import com.example.testapp.thread.CardThread;
import com.example.testapp.thread.LogThread;
import com.example.testapp.thread.WaitThread;

import java.util.List;

import static com.example.testapp.utils.ConstantManager.FIRST_LAUNCH;

public class IntroActivity extends AppCompatActivity {

    private ImageView introImage;
    private EditText countGeneratedCards;
    private Button introNextBtn, introDoneBtn;
    private int slideCount = 0;
    private Thread mCardThread;
    private String userInput;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_intro);

        introImage = findViewById(R.id.introImage);
        introImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_android_light_opacity_50));


        introNextBtn = findViewById(R.id.introNextBtn);
        introDoneBtn = findViewById(R.id.introDoneBtn);
        countGeneratedCards = findViewById(R.id.countGeneratedCards);
        countGeneratedCards.setHint(getResources().getString(R.string.count));

        introNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ++slideCount;

                if (slideCount == 1){
                    introImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_android_dark_opacity_50));
                    setHint();
                }

                if (slideCount == 2){
                    introImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_android_blue_opacity_100));
                    introNextBtn.setVisibility(View.GONE);
                    introDoneBtn.setVisibility(View.VISIBLE);
                    setHint();
                }
            }
        });

        introDoneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                introDoneBtn.setVisibility(View.GONE);
                countGeneratedCards.setVisibility(View.GONE);
                int countGenerated = 5;
                userInput = countGeneratedCards.getText().toString();
                if (!TextUtils.isEmpty(userInput)){
                    countGenerated = Integer.parseInt(userInput);
                }
                List<Card> generatedCards = generateCards(countGenerated);
                saveCardsToDb(generatedCards);
                avoidIntroLaunch();

                Thread mWaitThread = new WaitThread(IntroActivity.this, "waitThread", mCardThread);
                mWaitThread.start();
            }
        });
    }

    private void setHint() {
        userInput = countGeneratedCards.getText().toString();
        if (TextUtils.isEmpty(userInput)){
            countGeneratedCards.setHint(getResources().getString(R.string.default_count));
        }
    }

    public List<Card> generateCards(int count){
        DummyData dummyData = new DummyData(this, count);
        return dummyData.generateDummyCardList();
    }

    public void saveCardsToDb(final List<Card> list){
        mCardThread = new CardThread("cardThread", list);
        Thread mLogThread = new LogThread("logThread", list);
        mCardThread.start();
        mLogThread.start();
    }


    private void avoidIntroLaunch() {
        SharedPreferences getPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        SharedPreferences.Editor e = getPrefs.edit();
        e.putBoolean(FIRST_LAUNCH, true);
        e.apply();
    }

    public static void start(@NonNull Activity activity) {
        activity.startActivity(new Intent(activity, MainActivity.class));
        activity.finish();
    }
}

