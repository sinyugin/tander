package com.example.testapp.db;


public class Memento {

    private Save save;

    public Memento(Save save){
        this.save = save;
    }

    public Save getSave() {
        return save;
    }
}
