package com.example.testapp.db;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.example.testapp.model.Card;

import static com.example.testapp.utils.ConstantManager.DATE;
import static com.example.testapp.utils.ConstantManager.DELETED;
import static com.example.testapp.utils.ConstantManager.ID;
import static com.example.testapp.utils.ConstantManager.PROGRESS;
import static com.example.testapp.utils.ConstantManager.STATUS;
import static com.example.testapp.utils.ConstantManager.TAPPED;
import static com.example.testapp.utils.ConstantManager.TITLE;


public class CardCursorWrapper extends CursorWrapper {

    public CardCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Card getCard(){

        String uuid = getString(getColumnIndex(ID));
        String title = getString(getColumnIndex(TITLE));
        String progress = getString(getColumnIndex(PROGRESS));
        String date = getString(getColumnIndex(DATE));
        String status = getString(getColumnIndex(STATUS));
        String deleted = getString(getColumnIndex(DELETED));
        String tapped = getString(getColumnIndex(TAPPED));

        Card card = new Card(uuid);
        if (title != null) card.setTitle(title);
        if (progress != null) card.setProgress(progress);
        if (date != null) card.setDate(date);
        if (status != null) card.setStatus(status);
        if (deleted != null) card.setDeleted(deleted);
        if (tapped != null) card.setTapped(tapped);

        return card;
    }
}
