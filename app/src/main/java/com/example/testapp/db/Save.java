package com.example.testapp.db;

public class Save {

    final private String uuid,
                        title,
                        progress,
                        date,
                        status,
                        tapped,
                        deleted;

    public Save(String uuid, String title, String progress, String date, String status, String tapped, String deleted) {
        this.uuid = uuid;
        this.title = title;
        this.progress = progress;
        this.date = date;
        this.status = status;
        this.tapped = tapped;
        this.deleted = deleted;
    }

    public String getUuid() {
        return uuid;
    }

    public String getTitle() {
        return title;
    }

    public String getProgress() {
        return progress;
    }

    public String getDate() {
        return date;
    }

    public String getStatus() {
        return status;
    }

    public String getTapped() {
        return tapped;
    }

    public String getDeleted() {
        return deleted;
    }
}
