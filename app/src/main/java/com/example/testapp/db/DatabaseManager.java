package com.example.testapp.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.testapp.model.Card;

import java.util.ArrayList;
import java.util.List;

import static com.example.testapp.utils.ConstantManager.CARD_TABLE;
import static com.example.testapp.utils.ConstantManager.CARD_TABLE_LOG;
import static com.example.testapp.utils.ConstantManager.DATE;
import static com.example.testapp.utils.ConstantManager.DELETED;
import static com.example.testapp.utils.ConstantManager.ID;
import static com.example.testapp.utils.ConstantManager.PROGRESS;
import static com.example.testapp.utils.ConstantManager.STATUS;
import static com.example.testapp.utils.ConstantManager.TAPPED;
import static com.example.testapp.utils.ConstantManager.TITLE;


public class DatabaseManager {

    private Context context;

    private static int sOpenCount = 0;

    private static DatabaseManager instance;
    private SQLiteOpenHelper databaseHelper;
    private SQLiteDatabase writeDatabase;


    private DatabaseManager(Context context, SQLiteOpenHelper helper) {
        this.context = context.getApplicationContext();
        databaseHelper = helper;
    }

    public static synchronized void initializeInstance(Context c, SQLiteOpenHelper helper){
        if (instance == null){
            instance = new DatabaseManager(c, helper);
        }
    }

    public static synchronized DatabaseManager getInstance(){
        if (instance == null){
            throw new IllegalStateException(DatabaseManager.class.getSimpleName() + "haven't initialized");
        }
        return instance;
    }

    private synchronized SQLiteDatabase openWritableDatabase(){
        if (sOpenCount == 0){
            writeDatabase = databaseHelper.getWritableDatabase();
        }
        ++sOpenCount;
        return writeDatabase;
    }

    private synchronized void closeWriteDatabase(){
        --sOpenCount;
        if (sOpenCount == 0 ){
            writeDatabase.close();
        }
    }

    private static ContentValues getContentValues(Card card){
        ContentValues values = new ContentValues();
        values.put(ID, card.getUuid());
        values.put(TITLE, card.getTitle());
        values.put(PROGRESS, card.getProgress());
        values.put(DATE, card.getDate());
        values.put(STATUS, card.getStatus());
        values.put(DELETED, card.getDeleted());
        values.put(TAPPED, card.getTapped());
        return values;
    }

    //CARD_TABLE single insert
    public void insertCard(Card card) {
        writeDatabase = openWritableDatabase();
        ContentValues values = getContentValues(card);
        writeDatabase.insert(CARD_TABLE, null, values);
        closeWriteDatabase();
    }

    //CARD_TABLE_LOG single insert
    public void insertCardLog(Card card) {
        writeDatabase = openWritableDatabase();
        ContentValues values = getContentValues(card);
        writeDatabase.insert(CARD_TABLE_LOG, null, values);
        closeWriteDatabase();
    }

    //CARD_TABLE multi insert
    public void insertCardList(List<Card> list) {
        writeDatabase = openWritableDatabase();
        int i = 0;
        for (Card card : list){
            ContentValues values = getContentValues(card);
            writeDatabase.insert(CARD_TABLE, null, values);
        }
        closeWriteDatabase();
    }

    //CARD_TABLE_LOG multi insert
    public void insertCardListLog(List<Card> list) {
        writeDatabase = openWritableDatabase();
        int i = 0;
        for (Card card : list){
            ContentValues values = getContentValues(card);
            writeDatabase.insert(CARD_TABLE_LOG, null, values);
        }
        closeWriteDatabase();
    }

    //CARD_TABLE
    public void updateCard(Card card){
        writeDatabase = openWritableDatabase();
        String uuid = card.getUuid();
        ContentValues values = getContentValues(card);
        writeDatabase.update(CARD_TABLE, values, ID + " = ?", new String[] {uuid});
    }

    //drag and drop
    public void removeCards(Card card){
        writeDatabase = openWritableDatabase();
        String uuid  = card.getUuid();
        writeDatabase.delete(CARD_TABLE, ID + " = ?", new String[] {uuid});
    }

    //CARD_TABLE_LOG delete card sing history record
    public void removeCardLog(Card card){
        writeDatabase = openWritableDatabase();
        String uuid  = card.getUuid();
        String date  = card.getDate();
        String deleted  = card.getDeleted();
        writeDatabase.delete(CARD_TABLE_LOG,
                ID + " = ?  AND " + DATE + " = ? AND " + DELETED + " = ?",
                new String[] {uuid, date, deleted});
    }

    //delete all for one card
    public void removeCard(Card card){
        writeDatabase = openWritableDatabase();
        String uuid  = card.getUuid();
        writeDatabase.delete(CARD_TABLE, ID + " = ?", new String[] {uuid});
        writeDatabase.delete(CARD_TABLE_LOG, ID + " = ?", new String[] {uuid});
    }

    private CardCursorWrapper queryItems(String whereClause, String[] whereArgs){
        writeDatabase = openWritableDatabase();
        Cursor cursor = writeDatabase.query(
                CARD_TABLE,
                null,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );
        return new CardCursorWrapper(cursor);
    }

    private CardCursorWrapper queryItemLogs(String whereClause, String[] whereArgs){
        writeDatabase = openWritableDatabase();
        Cursor cursor = writeDatabase.query(
                CARD_TABLE_LOG,
                null,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );
        return new CardCursorWrapper(cursor);
    }

    public ArrayList<Card> getCards(){
        ArrayList<Card> cardList = new ArrayList<>();
        CardCursorWrapper cursor = new CardCursorWrapper(queryItems(DELETED + " = ?", new String[]{"false"}));

        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()){
                Card card = cursor.getCard();
                cardList.add(card);
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return cardList;
    }

    public ArrayList<Card> getCardLogs(String uuid){
        ArrayList<Card> cardListLog = new ArrayList<>();
        CardCursorWrapper cursor = new CardCursorWrapper(queryItemLogs(ID + " = ?", new String[]{uuid}));

        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()){
                Card card = cursor.getCard();
                cardListLog.add(card);
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return cardListLog;
    }

    public ArrayList<Card> getDeletedCards(){
        ArrayList<Card> cardList = new ArrayList<>();
        CardCursorWrapper cursor = new CardCursorWrapper(queryItems(DELETED + " = ?", new String[]{"true"}));

        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()){
                cardList.add(cursor.getCard());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }

        return cardList;
    }

    public Card getCardByUuid(String uuid){
        CardCursorWrapper cursor = new CardCursorWrapper(queryItems(ID + " = ?", new String[] {uuid}));
        try {
            if (cursor.getCount() == 0) return null;
            cursor.moveToFirst();
            return cursor.getCard();
        } finally {
            cursor.close();
        }
    }

    //Restart App
    public void removeAllCards(){
        writeDatabase = openWritableDatabase();
        writeDatabase.delete(CARD_TABLE, null, null);
        writeDatabase.delete(CARD_TABLE_LOG, null, null);
        closeWriteDatabase();
    }
}
