package com.example.testapp.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.example.testapp.utils.ConstantManager.CARD_TABLE;
import static com.example.testapp.utils.ConstantManager.CARD_TABLE_LOG;
import static com.example.testapp.utils.ConstantManager.CARD_TABLE_VERSION;
import static com.example.testapp.utils.ConstantManager.DATE;
import static com.example.testapp.utils.ConstantManager.DB_NAME;
import static com.example.testapp.utils.ConstantManager.DELETED;
import static com.example.testapp.utils.ConstantManager.ID;
import static com.example.testapp.utils.ConstantManager.PROGRESS;
import static com.example.testapp.utils.ConstantManager.STATUS;
import static com.example.testapp.utils.ConstantManager.TAPPED;
import static com.example.testapp.utils.ConstantManager.TITLE;


public class ItemBaseHelper extends SQLiteOpenHelper {

    private static final String CREATE_CARD_TABLE = "CREATE TABLE " + CARD_TABLE +
            " (" +
            " _id integer primary key autoincrement, " +
            ID + ", " +
            TITLE + ", " +
            PROGRESS + ", " +
            DATE + ", " +
            STATUS + ", " +
            DELETED + ", " +
            TAPPED +
            " )";

    private static final String CREATE_CARD_TABLE_LOG = "CREATE TABLE " + CARD_TABLE_LOG +
            " (" +
            " _id integer primary key autoincrement, " +
            ID + ", " +
            TITLE + ", " +
            PROGRESS + ", " +
            DATE + ", " +
            STATUS + ", " +
            DELETED + ", " +
            TAPPED +
            " )";


    public ItemBaseHelper(Context context) {
        super(context,DB_NAME, null, CARD_TABLE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_CARD_TABLE);
        sqLiteDatabase.execSQL(CREATE_CARD_TABLE_LOG);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

}
