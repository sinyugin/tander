package com.example.testapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;

import com.example.testapp.adapter.CardAdapter;
import com.example.testapp.db.DatabaseManager;
import com.example.testapp.model.Card;
import com.example.testapp.utils.AlertDialogFactory;
import com.example.testapp.utils.ConstantManager;
import com.example.testapp.utils.SwipeDragDropHelper;
import com.example.testapp.utils.TappedListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

import static com.example.testapp.utils.ConstantManager.ACTION_MODE_STATE;
import static com.example.testapp.utils.ConstantManager.CARDS_ID;
import static com.example.testapp.utils.ConstantManager.CARDS_POSITION;
import static com.example.testapp.utils.ConstantManager.FIRST_LAUNCH;
import static com.example.testapp.utils.ConstantManager.OFF;
import static com.example.testapp.utils.ConstantManager.ON;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        TappedListener {

    private Toolbar toolbar;
    private DrawerLayout drawer;
    private FloatingActionButton fab;

    public static ArrayList<Card> cardList;
    private CardAdapter cardAdapter;
    private RecyclerView cardRV;

    private DatabaseManager databaseManager;
    private CoordinatorLayout coordinator;
    private AlertDialogFactory alertDialogFactory;


    public static boolean multiSelect = false;
    private ActionMode actionMode;
    private ArrayList<String> tappedCardsId = new ArrayList<>();
    private ArrayList<Integer> tappedCardsPosition = new ArrayList<>();
    private String actionModeState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(ConstantManager.TAG, "onCreate");
        supportRequestWindowFeature(Window.FEATURE_ACTION_MODE_OVERLAY);
        super.onCreate(savedInstanceState);

        firstLaunch();

        if (savedInstanceState != null) {
            tappedCardsId = savedInstanceState.getStringArrayList(CARDS_ID);
            tappedCardsPosition = savedInstanceState.getIntegerArrayList(CARDS_POSITION);
        }

        setContentView(R.layout.activity_main);

        alertDialogFactory = new AlertDialogFactory();
        coordinator = findViewById(R.id.coordinator);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideFab();
                Snackbar snackbar = Snackbar.make(v, R.string.add_new_card, Snackbar.LENGTH_SHORT);
                snackbar.setAction(R.string.positive_answer, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogFactory.addCardDialog(MainActivity.this, coordinator, cardAdapter, cardList, fab, -1);
                    }
                });
                snackbar.addCallback(new Snackbar.Callback() {
                    @Override
                    public void onDismissed(Snackbar transientBottomBar, int event) {
                        super.onDismissed(transientBottomBar, event);
                        showFab();
                    }
                });
                snackbar.show();
            }
        });

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        databaseManager = DatabaseManager.getInstance();
        cardList = databaseManager.getCards();

        cardAdapter = new CardAdapter(this, cardList, tappedCardsId, this);
        cardRV = findViewById(R.id.cardRV);
        cardRV.setLayoutManager(new LinearLayoutManager(this));
        cardRV.setHasFixedSize(true);
        cardRV.setAdapter(cardAdapter);

        //Start Move and Swipe
        ItemTouchHelper.SimpleCallback swipeDragDropHelper = new SwipeDragDropHelper(
                3,
                48,
                cardList,
                cardAdapter,
                MainActivity.this,
                coordinator,
                fab);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(swipeDragDropHelper);
        itemTouchHelper.attachToRecyclerView(cardRV);
        //End Move and Swipe
    }

    private void firstLaunch() {
        SharedPreferences getPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        boolean isFirstLaunch = getPrefs.getBoolean(FIRST_LAUNCH, false);
        if (!isFirstLaunch) {
            showIntroActivity();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(ConstantManager.TAG, "onResume");
        cardList = databaseManager.getCards();
//        for (Card c : cardList){
//            databaseManager.updateCard(c);
//        }
        cardAdapter.setList(cardList);
    }

    @Override
    protected void onStart() {
        Log.d(ConstantManager.TAG, "onStart");
        super.onStart();
    }

    @Override
    protected void onStop() {
        Log.d(ConstantManager.TAG, "onStop");
        super.onStop();
    }

    @Override
    protected void onPause() {
        Log.d(ConstantManager.TAG, "onPause");
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        Log.d(ConstantManager.TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    protected void onRestart() {
        Log.d(ConstantManager.TAG, "onRestart");
        super.onRestart();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);

        MenuItem search = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        searchDataInCard(searchView);

        return true;
    }

    private void searchDataInCard(SearchView searchView) {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                cardAdapter.getFilter().filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                cardAdapter.getFilter().filter(newText);
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.deleteCard:
                startActivity(new Intent(this, BinActivity.class));
                break;
            case R.id.backToStart:
                SharedPreferences getPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                SharedPreferences.Editor e = getPrefs.edit();
                e.putBoolean(FIRST_LAUNCH, false);
                e.apply();
                startActivity(new Intent(this, IntroActivity.class));
                DatabaseManager.getInstance().removeAllCards();
                finish();
        }
        return true;
    }


    //Start Action mode
    private ActionMode.Callback actionModeCallbacks = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            multiSelect = true;
            menu.add(R.string.delete_selected_cards);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            Collections.sort(tappedCardsPosition, Collections.reverseOrder());
            for (Integer position : tappedCardsPosition){
                cardAdapter.removeCard(position);
            }
            for (String uuid : tappedCardsId){
                Card card = DatabaseManager.getInstance().getCardByUuid(uuid);
                card.setDate(new SimpleDateFormat("hh:mm:ss, dd MMM yyyy", Locale.getDefault()).format(new Date()));
                card.setStatus(getResources().getString(R.string.status_deleted));
                card.setDeleted("true");
                DatabaseManager.getInstance().updateCard(card);
                DatabaseManager.getInstance().insertCardLog(card);
                cardList.remove(card);
            }
            mode.finish();
            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            multiSelect = false;
            tappedCardsId.clear();
            tappedCardsPosition.clear();
            cardAdapter.notifyDataSetChanged();
            actionMode = null;
            actionModeState = OFF;
            showFab();
            SwipeDragDropHelper.drag = 3;
            SwipeDragDropHelper.swipe = 48;
        }
    };

    @Override
    public void onTapped(String uuid, int position) {

        if (actionMode == null){
            actionMode = startSupportActionMode(actionModeCallbacks);
            actionModeState = ON;
            hideFab();
            SwipeDragDropHelper.drag = 0;
            SwipeDragDropHelper.swipe = 0;
            multiSelect = true;
        }

        if (!tappedCardsId.contains(uuid)) {
            tappedCardsId.add(uuid);
            tappedCardsPosition.add(position);
        } else {
            tappedCardsId.remove(uuid);
            tappedCardsPosition.remove((Object)position);
            if (tappedCardsId.size() == 0){
                actionMode.finish();
            }
        }

        if (tappedCardsId.size()!=0){
            setTitleActionMode();
        }
    }

    private void setTitleActionMode() {
        int count = tappedCardsId.size();
        Resources resources = getResources();
        String title_action_mode = resources.getQuantityString(R.plurals.cards_action_mode, count, count);
        if (count == 1){
            actionMode.setTitle("Выбрана " + title_action_mode);
        } else {
            actionMode.setTitle("Выбраны " + title_action_mode);
        }
    }

    //End Action mode

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(ACTION_MODE_STATE, actionModeState);
        outState.putStringArrayList(CARDS_ID, tappedCardsId);
        outState.putIntegerArrayList(CARDS_POSITION, tappedCardsPosition);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        String state = savedInstanceState.getString(ACTION_MODE_STATE);
        if (state != null && state.equals(ON)) {
            actionMode = startSupportActionMode(actionModeCallbacks);
            actionModeState = ON;
            hideFab();
            setTitleActionMode();
        }
    }

    public void showFab(){
        fab.setVisibility(View.VISIBLE);
    }

    public void hideFab(){
        fab.setVisibility(View.INVISIBLE);
    }

    public void showIntroActivity(){
        Intent introActivity = new Intent(MainActivity.this, IntroActivity.class );
        startActivity(introActivity);
        finish();
    }
}
