package com.example.testapp.utils;


public interface ConstantManager {

    String TAG = "AppTander";

    //First Launch
    String FIRST_LAUNCH = "first_launch";


    //Action Mode
    String CARDS_ID = "tappedCardsId";
    String CARDS_POSITION = "tappedCardsPosition";
    String ACTION_MODE_STATE = "state";
    String ON = "on";
    String OFF = "off";


    String DB_NAME = "AppTander.db";
    String CARD_TABLE = "card_table";
    String CARD_TABLE_LOG = "card_table_log";
    int CARD_TABLE_VERSION = 1;
    String ID = "uuid";
    String TITLE = "title";
    String PROGRESS = "progress";
    String DATE = "date";
    String STATUS = "statud";
    String DELETED = "deleted";
    String TAPPED = "tapped";
}
