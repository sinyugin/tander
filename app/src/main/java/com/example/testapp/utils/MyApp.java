package com.example.testapp.utils;

import android.app.Application;

import com.example.testapp.db.DatabaseManager;
import com.example.testapp.db.ItemBaseHelper;
import com.facebook.stetho.Stetho;


public class MyApp extends Application {
    public void onCreate() {
        super.onCreate();
        DatabaseManager.initializeInstance(getApplicationContext(), new ItemBaseHelper(getApplicationContext()));
        Stetho.initializeWithDefaults(this);
    }
}
