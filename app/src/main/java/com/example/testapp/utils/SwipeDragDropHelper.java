package com.example.testapp.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.example.testapp.R;
import com.example.testapp.adapter.CardAdapter;
import com.example.testapp.db.DatabaseManager;
import com.example.testapp.db.Memento;
import com.example.testapp.db.Save;
import com.example.testapp.model.Card;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import static android.support.v7.widget.helper.ItemTouchHelper.START;


public class SwipeDragDropHelper extends ItemTouchHelper.SimpleCallback {

    private Paint paintBackground = new Paint();
    private Paint paintText = new Paint();
    private ArrayList<Card> cardList;
    private CardAdapter cardAdapter;
    private Context context;
    private CoordinatorLayout coordinator;
    private FloatingActionButton fab;
    private DatabaseManager databaseManager;

    public static int drag = 3;
    public static int swipe = 48;

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        return makeMovementFlags(drag, swipe);
    }

    public SwipeDragDropHelper(int dragDirs, int swipeDirs,
                               ArrayList<Card> cardList,
                               CardAdapter cardAdapter,
                               Context context,
                               CoordinatorLayout coordinator,
                               FloatingActionButton fab) {

        super(dragDirs, swipeDirs);
        this.cardList = cardList;
        this.cardAdapter = cardAdapter;
        this.context = context;
        this.coordinator = coordinator;
        this.fab = fab;
        databaseManager = DatabaseManager.getInstance();
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, final float dX, float dY, int actionState, boolean isCurrentlyActive) {
        View itemView = viewHolder.itemView;
        float left = itemView.getLeft();
        float right = itemView.getRight();
        float top = itemView.getTop();
        float bottom = itemView.getBottom();

        paintText.setColor(Color.parseColor("#FFFFFF"));

        RectF background;
        if(actionState == ItemTouchHelper.ACTION_STATE_SWIPE){
            float corners = 16;
            if(dX > 0){
                background = new RectF(left, top, right, bottom);
                paintBackground.setColor(Color.parseColor("#388E3C"));
                c.drawRoundRect(background, corners, corners, paintBackground);
                drawText("Редактирование", c, background, paintText);
            }
            if (dX < 0){
                background = new RectF(right, top, left, bottom);
                paintBackground.setColor(Color.parseColor("#D32F2F"));
                c.drawRoundRect(background, corners, corners, paintBackground);
                drawText("Удаление", c, background, paintText);
            }
        }

        if(actionState == ItemTouchHelper.ACTION_STATE_DRAG){
            background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX,(float) itemView.getBottom());
            c.drawRect(background, paintBackground);
        }

        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
    }

    private void drawText(String text, Canvas c, RectF background, Paint p) {
        float textSize = 60;
        p.setColor(Color.WHITE);
        p.setAntiAlias(true);
        p.setTextSize(textSize);

        float textWidth = p.measureText(text);
        c.drawText(text, background.centerX()-(textWidth /2), background.centerY()+(textSize/2), p);
    }


    @Override
    public boolean onMove(RecyclerView recyclerView,
                          RecyclerView.ViewHolder viewHolder,
                          RecyclerView.ViewHolder target) {
        int currentPosition = viewHolder.getAdapterPosition();
        int newPosition = target.getAdapterPosition();

        Card card = cardList.get(currentPosition);
        cardList.remove(currentPosition);
        cardList.add(newPosition, card);

        //удаление всего списка и повторная запись списка для обработки drag and drop
        for (Card c: cardList){
            databaseManager.removeCards(c);
        }
        for (Card c: cardList){
            databaseManager.insertCard(c);
        }
        cardAdapter.notifyItemMoved(currentPosition,newPosition);
        return true;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        AlertDialogFactory alertDialogFactory = new AlertDialogFactory();
        final int position = viewHolder.getAdapterPosition();

        final Memento memento = setMemento(position);

        final Card card = cardList.get(position);

        if (direction == START){
            cardList.remove(card);
            card.setDate(new SimpleDateFormat("hh:mm:ss, dd MMM yyyy", Locale.getDefault()).format(new Date()));
            card.setStatus(context.getResources().getString(R.string.status_deleted));
            card.setDeleted("true");
            databaseManager.updateCard(card);
            databaseManager.insertCardLog(card);
            cardAdapter.removeCard(position);

            Snackbar.make(coordinator, "Отменить удаление?", Snackbar.LENGTH_SHORT)
                    .setAction("Да", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Card c = getMemento(memento);
                            cardList.add(position, c);
                            cardAdapter.addCard(position, c);
                            databaseManager.updateCard(c);
                            databaseManager.removeCardLog(card);
                        }
                    })
                    .show();
            }
        else {
            alertDialogFactory.editCardDialog(context, coordinator, cardAdapter, cardList, fab, position);
            cardAdapter.notifyItemChanged(position);
        }
    }

    @NonNull
    private Card getMemento(Memento memento) {
        String uuid = memento.getSave().getUuid();
        String title = memento.getSave().getTitle();
        String progress = memento.getSave().getProgress();
        String date = memento.getSave().getDate();
        String status = memento.getSave().getStatus();
        String tapped = memento.getSave().getTapped();
        String deleted = memento.getSave().getDeleted();

        Card c = new Card(uuid);
        c.setTitle(title);
        c.setProgress(progress);
        c.setDate(date);
        c.setStatus(status);
        c.setTapped(tapped);
        c.setDeleted(deleted);
        return c;
    }

    @NonNull
    private Memento setMemento(int position) {
        final Card proxy = cardList.get(position);
        return new Memento(new Save(
                proxy.getUuid(),
                proxy.getTitle(),
                proxy.getProgress(),
                proxy.getDate(),
                proxy.getStatus(),
                proxy.getTapped(),
                proxy.getDeleted()));
    }

}
