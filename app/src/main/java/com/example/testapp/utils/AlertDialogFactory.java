package com.example.testapp.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.example.testapp.R;
import com.example.testapp.adapter.CardAdapter;
import com.example.testapp.db.DatabaseManager;
import com.example.testapp.model.Card;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import static com.example.testapp.utils.ConstantManager.TAG;


public class AlertDialogFactory {

    private View view;
    private AlertDialog.Builder dialog;
    private EditText cardTitle, cardProgress;
    private CoordinatorLayout coordinator;
    private ArrayList<Card> cardList;
    private CardAdapter cardAdapter;
    private FloatingActionButton cardFab;
    private int position;
    private Context context;
    private Card card;

    public void addCardDialog(final Context context,
                              final CoordinatorLayout coordinatorLayout,
                              final CardAdapter adapter,
                              final ArrayList<Card> list,
                              final FloatingActionButton fab,
                              int editPosition){
        this.context = context;

        initMembers(context, coordinatorLayout, list, adapter, fab, editPosition);

        dialog.setTitle("Новая карточка");
        dialog.setMessage("Введите заголовок и прогресс");
        dialog.setView(view);
        setPositiveButton();
        setNegativeButton();
        dialog.show();

        setTitleTextWatcher();
        setProgressTextWatcher();
    }

    public void editCardDialog(final Context context,
                               final CoordinatorLayout coordinatorLayout,
                               final CardAdapter adapter,
                               final ArrayList<Card> list,
                               final FloatingActionButton fab,
                               int position){

        initMembers(context, coordinatorLayout, list, adapter, fab, position);
        cardTitle.setEnabled(false);
        cardTitle.setText(cardList.get(position).getTitle());

        dialog.setTitle("Редактирование карточки");
        dialog.setMessage("Введите прогресс");
        dialog.setView(view);
        setPositiveButton();
        setNegativeButton();
        dialog.show();

        setProgressTextWatcher();
    }

    private void initMembers(Context context,
                             CoordinatorLayout coordinatorLayout,
                             ArrayList<Card> list,
                             CardAdapter adapter,
                             FloatingActionButton fab,
                             int editPosition)
    {
        try {
            view = LayoutInflater.from(context).inflate(R.layout.dialog_layout, null);
        } catch (Exception e){
            Log.d(TAG, e.getLocalizedMessage());
        }
        cardTitle = view.findViewById(R.id.titleEdt);
        cardProgress = view.findViewById(R.id.progressEdt);
        dialog = new AlertDialog.Builder(context);
        coordinator = coordinatorLayout;
        cardList = list;
        cardAdapter = adapter;
        cardFab = fab;
        position = editPosition;

    }

    private void setPositiveButton(){
        dialog.setPositiveButton("Ок", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String title = cardTitle.getText().toString();
                String progress = cardProgress.getText().toString();
                if (!checkTitle()) return;
                if (!checkProgress()) return;

                if (position == -1){
                    card = new Card();
                    card.setStatus(view.getContext().getString(R.string.status_created));
                } else {
                    card = cardList.get(position);
                    if (card.getProgress().equals(progress))
                        return;
                    card.setStatus(view.getContext().getString(R.string.status_edited));
                }

                card.setTitle(title);
                card.setProgress(progress);
                String timeStamp = new SimpleDateFormat("hh:mm:ss, dd MMM yyyy", Locale.getDefault()).format(new Date());
                card.setDate(timeStamp);
                card.setDeleted("false");
                card.setTapped("false");

                if (position == -1){
                    cardAdapter.addCard(card);
                    //cardList.add(card);
                    DatabaseManager.getInstance().insertCard(card);
                    DatabaseManager.getInstance().insertCardLog(card);
                } else {
                    cardAdapter.setCard(position, card);
                    DatabaseManager.getInstance().updateCard(card);
                    DatabaseManager.getInstance().insertCardLog(card);
                }
                cardTitle.setVisibility(View.GONE);
                cardProgress.setVisibility(View.GONE);
                dialogInterface.dismiss();
            }
        });
    }

    private void setNegativeButton() {
        cardFab.setVisibility(View.VISIBLE);
        dialog.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
    }

    private void setTitleTextWatcher() {
        cardTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String ss = s.toString();
                for (Card card : cardList){
                    if (card.getTitle().toLowerCase().equals(ss.toLowerCase())){
                        cardTitle.setError("Введите уникальный заголовок");
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setProgressTextWatcher() {
        cardProgress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String ss = s.toString();
                try {
                    int progress_value = Integer.parseInt(ss);
                    if (progress_value < 0 || progress_value > 100){
                        cardProgress.setError("Введите прогресс от 0 до 100");
                    }
                } catch (RuntimeException e){
                    cardProgress.setError("Введите прогресс от 0 до 100");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private boolean checkTitle(){
        if (position == -1) {
            String title = cardTitle.getText().toString();
            if (TextUtils.isEmpty(title)) {
                Snackbar.make(coordinator, "Введите заголовок", Snackbar.LENGTH_LONG).show();
                return false;
            }

            for (Card card : cardList) {
                if (card.getTitle().toLowerCase().equals(title.toLowerCase())) {
                    Snackbar.make(coordinator, "Введите уникальный заголовок", Snackbar.LENGTH_LONG).show();
                    return false;
                }
            }
        }
        return true;
    }

    private boolean checkProgress(){
        String progress = cardProgress.getText().toString();
        if (TextUtils.isEmpty(progress)) {
            Snackbar.make(coordinator, "Введите прогресс", Snackbar.LENGTH_LONG).show();
            return false;
        }

        try {
            Integer.parseInt(progress);
        } catch (RuntimeException e){
            Snackbar.make(coordinator, "Введите прогресс от 0 до 100", Snackbar.LENGTH_LONG).show();
            return false;
        }

        if (Integer.parseInt(progress) < 0 || Integer.parseInt(progress) > 100){
            Snackbar.make(coordinator, "Введите прогресс от 0 до 100", Snackbar.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

}
