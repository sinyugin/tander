package com.example.testapp.utils;


public interface TappedListener {
    void onTapped(String uuid, int position);
}
