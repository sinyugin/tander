package com.example.testapp.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.testapp.R;
import com.example.testapp.db.DatabaseManager;
import com.example.testapp.model.Card;
import com.example.testapp.utils.ConstantManager;

import java.util.ArrayList;
import java.util.Collections;

import static com.example.testapp.utils.ConstantManager.ID;


public class DetailFragment extends Fragment {


    private Card card;
    private String uuid;
    private DatabaseManager cardLab;
    private TextView itemTitle, itemProgressTxv, itemStatus, itemDate;
    private ProgressBar yellowProgress;

    private ArrayList<Card> cardListLog;
    private RecyclerView cardRV;
    private DetailAdapter detailAdapter;

    public DetailFragment(){
    }

    public static DetailFragment newInstance(String uuid){
        Bundle args = new Bundle();
        args.putString(ID, uuid);
        DetailFragment fragment = new DetailFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(ConstantManager.TAG, "Fragment onAttach");
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(ConstantManager.TAG, "Fragment onCreate");
        Bundle args = getArguments();
        if (args != null){
            uuid = args.getString(ID);
            card = DatabaseManager.getInstance().getCardByUuid(uuid);
        }
        cardLab = DatabaseManager.getInstance();
        cardListLog = cardLab.getCardLogs(uuid);
        Collections.reverse(cardListLog);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View itemView = inflater.inflate(R.layout.fragment_detail_layout, container, false);
        Log.d(ConstantManager.TAG, "Fragment onCreateView");
        itemTitle = itemView.findViewById(R.id.itemNameTxv);
        itemStatus = itemView.findViewById(R.id.itemStatus);
        itemDate = itemView.findViewById(R.id.itemDate);
        itemProgressTxv = itemView.findViewById(R.id.itemProgressTxv);
        yellowProgress = itemView.findViewById(R.id.itemProgress);

        itemTitle.setText(card.getTitle());
        itemProgressTxv.setText(card.getProgress());
        yellowProgress.setProgress(Integer.parseInt(card.getProgress()));
        itemStatus.setText(card.getStatus());
        itemDate.setText(card.getDate());

        detailAdapter = new DetailAdapter();
        cardRV = itemView.findViewById(R.id.cardRV);
        cardRV.setHasFixedSize(true);
        cardRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        cardRV.setAdapter(detailAdapter);

        return itemView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(ConstantManager.TAG, "Fragment onActivityCreated");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(ConstantManager.TAG, "Fragment onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(ConstantManager.TAG, "Fragment onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(ConstantManager.TAG, "Fragment onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(ConstantManager.TAG, "Fragment onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(ConstantManager.TAG, "Fragment onDestroyView");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(ConstantManager.TAG, "Fragment onDestroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(ConstantManager.TAG, "Fragment onDetach");
    }

    class DetailAdapter extends RecyclerView.Adapter<DetailViewHolder>{



        @Override
        public DetailViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_view, parent, false);
            return new DetailViewHolder(view);
        }

        @Override
        public void onBindViewHolder(DetailViewHolder holder, int position) {
            if (position == 0){
                holder.itemContainer.setVisibility(View.GONE);
                return;
            }

            Card card = cardListLog.get(position);
            holder.itemTitle.setText(card.getTitle());
            holder.itemProgressTxv.setText(card.getProgress());
            holder.itemStatus.setText(card.getStatus());
            holder.itemDate.setText(card.getDate());
            int value = Integer.parseInt(card.getProgress());
            holder.yellowProgress.setProgress(value);
        }

        @Override
        public int getItemCount() {
            return cardListLog.size();
        }
    }

    private class DetailViewHolder extends RecyclerView.ViewHolder {

        private RelativeLayout itemContainer;
        private TextView itemTitle, itemProgressTxv, itemStatus, itemDate;
        private ProgressBar yellowProgress;

        public DetailViewHolder(View itemView) {
            super(itemView);
            itemContainer = itemView.findViewById(R.id.itemContainer);
            itemTitle = itemView.findViewById(R.id.itemNameTxv);
            itemStatus = itemView.findViewById(R.id.itemStatus);
            itemDate = itemView.findViewById(R.id.itemDate);
            itemProgressTxv = itemView.findViewById(R.id.itemProgressTxv);
            yellowProgress = itemView.findViewById(R.id.itemProgress);
        }
    }
}
