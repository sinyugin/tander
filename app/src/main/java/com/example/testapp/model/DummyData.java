package com.example.testapp.model;

import android.content.Context;
import android.support.annotation.NonNull;

import com.example.testapp.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Random;


public class DummyData {

    private Context context;
    private int count = 5;

    public DummyData(Context context, int count) {
        this.context = context;
        this.count = count;
    }

    public ArrayList<Card> generateDummyCardList() {
        Random random = new Random();

        ArrayList<String> titles = generateDummyTitle(random);
        ArrayList<String> progresses = generateDummyProgress(random);

        ArrayList<Card> cardList = new ArrayList<>();
        for (int i = 0; i < count; i++){
            int randTitle = random.nextInt(titles.size());
            int randProgress = random.nextInt(progresses.size());

            Card card = new Card();
            card.setTitle(titles.get(randTitle));
            card.setProgress(progresses.get(randProgress));
            card.setStatus(context.getResources().getString(R.string.status_created));
            String timeStamp = new SimpleDateFormat("hh:mm:ss, dd MMM yyyy", Locale.getDefault()).format(new Date());
            card.setDate(timeStamp);
            card.setDeleted("false");
            card.setTapped("false");

            cardList.add(card);
            titles.remove(randTitle);
        }
        return cardList;
    }

    @NonNull
    private ArrayList<String> generateDummyProgress(Random random) {
        ArrayList<String> progresses = new ArrayList<>();
        for (int i = 0; i < 150; i++){
            String progress = String.valueOf(random.nextInt(100));
            if (!progress.equals("0"))
                progresses.add(progress);
        }
        return progresses;
    }

    @NonNull
    private ArrayList<String> generateDummyTitle(Random random) {
        ArrayList<String> titles = new ArrayList<>();
        for (int i = 1; i <= 15000; i++){
            String title = "card " + String.valueOf(random.nextInt(20000));
            if (!titles.contains(title))
                titles.add(title);
        }
        return titles;
    }
}
