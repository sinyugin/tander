package com.example.testapp.model;

import java.util.UUID;

public class Card {

    private String  uuid,
                    title,
                    progress,
                    date,
                    status,
                    tapped,
                    deleted;


    public Card() {
        uuid = UUID.randomUUID().toString();
    }

    public Card(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTapped() {
        return tapped;
    }

    public void setTapped(String tapped) {
        this.tapped = tapped;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }
}
