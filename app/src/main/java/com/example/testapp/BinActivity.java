package com.example.testapp;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.testapp.db.DatabaseManager;
import com.example.testapp.model.Card;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


public class BinActivity extends AppCompatActivity {

    private DatabaseManager databaseManager;
    private ArrayList<Card> deletedCardList;
    private LinearLayout rootView;
    private DeletedCardAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bin);

        databaseManager = DatabaseManager.getInstance();
        deletedCardList = databaseManager.getDeletedCards();
        rootView = findViewById(R.id.root_view);
        RecyclerView cardRV = findViewById(R.id.cardRV);
        cardRV.setLayoutManager(new LinearLayoutManager(this));
        cardRV.setHasFixedSize(true);

        adapter = new DeletedCardAdapter();
        cardRV.setAdapter(adapter);
    }

    public class DeletedCardAdapter extends RecyclerView.Adapter<DeletedCardViewHolder>{

        int tappedCard;

        @Override
        public DeletedCardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_view, parent, false);
            return new DeletedCardViewHolder(view);
        }


        @Override
        public void onBindViewHolder(DeletedCardViewHolder holder, final int position) {
            Card card = deletedCardList.get(position);
            holder.itemTitle.setText(card.getTitle());
            holder.itemProgressTxv.setText(card.getProgress());
            holder.itemStatus.setText(card.getStatus());
            holder.itemDate.setText(card.getDate());
            int value = Integer.parseInt(card.getProgress());
            holder.yellowProgress.setProgress(value);
            holder.itemContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tappedCard = position;
                    Snackbar snackbar = Snackbar.make(rootView, R.string.restore_card, Snackbar.LENGTH_LONG);
                    snackbar.setAction("Да", restoreListener);
                    snackbar.addCallback(snackbarCallback);
                    snackbar.show();
                }
            });

            holder.itemContainer.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    tappedCard = position;
                    Snackbar snackbar = Snackbar.make(rootView, "Удалить навсегда?", Snackbar.LENGTH_LONG);
                    snackbar.setAction("Да", totalDeleteListener);
                    snackbar.show();
                    return true;
                }
            });
        }

        @Override
        public int getItemCount() {
            return deletedCardList.size();
        }

        View.OnClickListener restoreListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Card card = deletedCardList.get(tappedCard);
                card.setDeleted("false");
                String timeStamp = new SimpleDateFormat("hh:mm:ss, dd MMM yyyy", Locale.getDefault()).format(new Date());
                card.setDate(timeStamp);
                card.setStatus(getResources().getString(R.string.status_restored));
                DatabaseManager.getInstance().updateCard(card);
                DatabaseManager.getInstance().insertCardLog(card);
                deletedCardList.remove(tappedCard);
                notifyDataSetChanged();
            }
        };

        Snackbar.Callback snackbarCallback = new Snackbar.Callback(){
            @Override
            public void onDismissed(Snackbar transientBottomBar, int event) {
                if (event == Snackbar.Callback.DISMISS_EVENT_TIMEOUT ||
                        event == Snackbar.Callback.DISMISS_EVENT_MANUAL ){
                    Toast.makeText(BinActivity.this, "Запись не восстановлена", Toast.LENGTH_SHORT).show();
                }
            }
        };

        View.OnClickListener totalDeleteListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Card card = deletedCardList.get(tappedCard);
                deletedCardList.remove(card);
                databaseManager.removeCard(card);
                notifyDataSetChanged();
            }
        };
    }


    private class DeletedCardViewHolder extends RecyclerView.ViewHolder {

        private TextView itemTitle, itemProgressTxv, itemStatus, itemDate;
        private ProgressBar yellowProgress;
        private RelativeLayout itemContainer;

        private DeletedCardViewHolder(View itemView) {
            super(itemView);
            itemTitle = itemView.findViewById(R.id.itemNameTxv);
            itemStatus = itemView.findViewById(R.id.itemStatus);
            itemDate = itemView.findViewById(R.id.itemDate);
            itemProgressTxv = itemView.findViewById(R.id.itemProgressTxv);
            yellowProgress = itemView.findViewById(R.id.itemProgress);
            itemContainer = itemView.findViewById(R.id.itemContainer);
        }
    }
}
