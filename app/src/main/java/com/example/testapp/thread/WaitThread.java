package com.example.testapp.thread;

import android.support.v7.app.AppCompatActivity;

import com.example.testapp.IntroActivity;

public class WaitThread extends Thread {

    private AppCompatActivity mContext;
    private Thread mCardThread;


    public WaitThread(AppCompatActivity context, String name, Thread cardThread) {
        super(name);
        mContext = context;
        mCardThread = cardThread;
    }

    @Override
    public void run() {
        if (mCardThread.isAlive()) {
            try {
                mCardThread.join();
                IntroActivity.start(mContext);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
