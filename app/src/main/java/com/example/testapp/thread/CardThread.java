package com.example.testapp.thread;

import com.example.testapp.db.DatabaseManager;
import com.example.testapp.model.Card;

import java.util.List;

/**
 * Created by sinyu on 26.03.2018.
 */

public class CardThread extends Thread {

    private List<Card> mList;

    public CardThread(String name, List<Card> list) {
        super(name);
        mList = list;
    }

    @Override
    public void run() {
        DatabaseManager.getInstance().insertCardList(mList);
    }
}
